Adds the total physical weight of a commerce order as a token to the `commerce-order` token type.

I needed to be able to put a order's weight in the customer's email message and noticed there wasn't an easy way to do this so. I figured others could benefit from this simple set of functionality and figured I'd extract it into standalone module.

This is a very simple module that alters the `commerce-order` token type to include our custom `order-weight` token.

##How To Use
Anywhere `commerce-order` tokens are available, you can now also use `order-weight` to display a order's total weight.

##Module Dependencies
 * [token](https://drupal.org/project/token)
 * [physical](https://drupal.org/project/physical)
 * [commerce](https://drupal.org/project/commerce)